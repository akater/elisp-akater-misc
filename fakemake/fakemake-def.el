;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'akater-misc
  authors "Dima Akater"
  first-publication-year-as-string "2019"
  org-files-in-order `( "akater-misc-essentials"
                        "akater-misc-functional"
                        "akater-misc-macs"

                        "akater-misc-plists-macs"
                        "akater-misc-plists"

                        "akater-misc-buffers-macs"
                        "akater-misc-buffers"
                           
                        "akater-misc-ordering"
                        "akater-misc-features"
                           
                        "akater-misc-strings"
                        "akater-misc-files"

                        "akater-misc-rest"

                        ,@(when (memq 'ansi-color use-flags)
                           `("akater-misc-ansi"))
                        "akater-misc-heavy"
                        ;; "akater-misc-dragons"
                        "akater-misc")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda ()
              (require 'org-src-elisp-extras)
              (setq fakemake-ort-keep-going t)
              (with-current-buffer (find-file-noselect
                                    "akater-misc-rest.org")
                (org-src-elisp-extras-autozero-gensym-counter-enable))))
