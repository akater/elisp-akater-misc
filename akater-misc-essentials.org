# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: akater-misc-essentials
#+subtitle: Part of the =akater-misc= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle akater-misc-essentials.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'subr-x))   ; string-empty-p
(eval-when-compile (require 'mmxx-macros-introspection))
#+end_src

* Some predicates, using ~mmxx-macros-introspection~
#+begin_src elisp :results none
(define-predicates+
  consp boundp fboundp keywordp
  integerp stringp eq)
#+end_src

* string-nonempty-p
** Examples
*** Basic Examples
**** TEST-PASSED Check that string is non-empty
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-nonempty-p "")
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

**** TEST-PASSED The string in question is returned for “true”
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-nonempty-p "wow")
#+end_src

#+EXPECTED:
#+begin_example elisp
"wow"
#+end_example

**** TEST-PASSED Whitespace string means non-empty string
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-nonempty-p " ")
#+end_src

#+EXPECTED:
#+begin_example elisp
" "
#+end_example

** Definition
#+begin_src elisp :results none
(defun string-nonempty-p (string)
  (declare (pure t) (side-effect-free t))
  (unless (string-empty-p string) string))
#+end_src

* Some predicates, not using ~mmxx-macros-introspection~ unfortunately
** string-equal+
#+begin_src elisp :results none
(defun string-equal+ (s1 s2)
  (declare (pure t) (side-effect-free t))
  (when (string-equal s1 s2) s1))
#+end_src

* no-hash-key
#+begin_src elisp :results none
(defconst no-hash-key (make-symbol "!:NO-HASH-KEY"))
#+end_src

* DEPRECATED Type ~keyword~
- State "DEPRECATED" from              [2020-09-07 Mon 22:33]
** Examples
*** Basic Examples
**** TEST-PASSED Keywords in Emacs Lisp are symbols which names start with a colon
See [[info:elisp#Symbol type]].
#+begin_src elisp :tangle no :results code :wrap example elisp
(cl-typep :x 'keyword)
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

*** Scope
**** TEST-PASSED A symbol name consisting of a single colon is a valid symbol name for a keyword
#+begin_src elisp :tangle no :results code :wrap example elisp
(cl-typep : 'keyword)
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

** Definition
#+begin_src elisp :results none
(when (< emacs-major-version 28)
  (cl-deftype keyword () '(and symbol (satisfies keywordp))))
#+end_src

* Function ~keyword~
** Summary
#+begin_example elisp :tangle no :eval never
(keyword x)
#+end_example
Convert /x/ to keyword.

** Examples
*** Basic Examples
**** TEST-PASSED Keyword from symbol
#+begin_src elisp :tangle no :results code :wrap example elisp
(keyword 'wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
:wow
#+end_example

**** TEST-PASSED Keyword from string
#+begin_src elisp :tangle no :results code :wrap example elisp
(keyword "wow")
#+end_src

#+EXPECTED:
#+begin_example elisp
:wow
#+end_example

*** Properties and Relations
**** TEST-PASSED Keyword returns its argument when it is a keyword
#+begin_src elisp :tangle no :results code :wrap example elisp
(keyword :wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
:wow
#+end_example

** Definition
#+begin_src elisp :results none
(cl-eval-when (compile load)
  (defsubst keyword (symbol-or-string)
    "Convert SYMBOL-OR-STRING to keyword."
    (cl-etypecase symbol-or-string
      (keyword symbol-or-string)
      ((or symbol string) (ifmt ":%s" symbol-or-string)))))
#+end_src

* dekeyword
** Examples
*** Basic Examples
**** TEST-PASSED Dekeyword a keyword
#+begin_src elisp :tangle no :results code :wrap example elisp
(dekeyword :wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
wow
#+end_example

**** TEST-PASSED A non-keyword symbol is dekeyworded to itself
#+begin_src elisp :tangle no :results code :wrap example elisp
(dekeyword 'wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
wow
#+end_example

** Definition
#+begin_src elisp :results none
(defsubst dekeyword (symbol)
  "Given SYMBOL, return symbol with name as in SYMBOL but normalized:
namely, without the leading colon, if it is present."
  (declare (pure t))
  (cl-etypecase symbol
    (keyword (intern (substring (symbol-name symbol) 1)))
    (symbol symbol)))
#+end_src

* ascii-char-p
** Definition
#+begin_src elisp :results none
(defun ascii-char-p (c)
  ;; ASCII_CHAR_P is defined (exactly this way) in lisp.h
  ;; but is inaccessible
  (declare (pure t) (side-effect-free t))
  (and (<= 0 c) (< c #x80)))
#+end_src

* ensure-string
** Examples
*** Basic Examples
**** TEST-PASSED Convert a number to string
This would normally be the most common case, as other cases have other functions covering them.
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-ensure-string 1)
#+end_src

#+EXPECTED:
#+begin_example elisp
"1"
#+end_example

**** TEST-PASSED Convert a symbol to string
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-ensure-string 'wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
"wow"
#+end_example

*** Scope
**** TEST-PASSED Keywords are treated specially for convenience
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-ensure-string :wow)
#+end_src

#+EXPECTED:
#+begin_example elisp
"wow"
#+end_example

** Definition
#+begin_src elisp :results none
(defun akater-misc-ensure-string (x)
  (declare (pure t) (side-effect-free t))
  (cl-etypecase x
    (number (number-to-string x))
    (keyword (substring (symbol-name x) 1))
    (symbol (symbol-name x))
    (string x)))
#+end_src

* ifmt
** Definition
#+begin_src elisp :results none
(defsubst ifmt (s &rest objects) (intern (apply #'format s objects)))
#+end_src

* constantly
#+begin_src elisp :results none
(defun constantly (value)
  (declare (pure t) (side-effect-free t))
  (lambda (&rest _) value))
#+end_src

* ensure-function
Taken (adapted) from Common Lisp's ASDF.
#+begin_src elisp :results none
(defun akater-misc-ensure-function (fun)
  "Coerce the object FUN into a function.

If FUN is a function, return it.

If the FUN is a non-sequence literal constant, return constantly
that, i.e. for a boolean keyword character or number.

Otherwise if FUN is a non-literally constant symbol, return its
`symbol-function'.

If FUN is a `cons', return the function that applies its `car' to
the appended list of the rest of its `cdr' and the arguments,
unless the `car' is LAMBDA, in which case the expression is
evaluated.

If FUN is a string, read a form from it and `eval' that in
a (cl-function ..) context."
  (cl-etypecase fun
    (function fun)
    ((or boolean keyword character number) (constantly fun))
    (hash-table (lambda (x) (gethash x fun no-hash-key)))
    (symbol (symbol-function fun))
    (cons (if (eq 'lambda (car fun))
              (eval fun
                    ;; maybe we should eval lexically here
                    )
            (lambda (&rest args) (apply (car fun) (append (cdr fun) args)))))
    (string (eval `(cl-function ,(car (read-from-string fun)))))))
#+end_src

* ensure-list
** Definition
#+begin_src elisp :results none
(defun akater-misc-ensure-list (x)
  (declare (pure t) (side-effect-free t))
  (if (listp x) x (list x)))
#+end_src

* cl-copy-tree-that-is-macro-argument
** Definition
#+begin_src elisp :results none
(defalias 'cl-copy-tree-that-is-macro-argument 'copy-tree
  "Copy tree submitted to a macro because it is undefined behavior
to modify macro arguments.")
#+end_src

* compound-form
Not possibru:
#+begin_src elisp :tangle no :results none
(cl-deftype compound-form () `(cons t list))
#+end_src

thus unfortunately
#+begin_src elisp :results none
(cl-deftype compound-form () `cons)
#+end_src

* Unicode Inequalities
#+begin_src elisp :results none
(defalias '≤ '<=)
(defalias '≥ '>=)
#+end_src

* with-memoization
#+begin_src elisp :results none
(eval-and-compile
  (if (<= 29 emacs-major-version) (require 'subr-x)
    (require 'cl-generic)
    ;; (require 'gv)
    (defalias 'with-memoization 'cl--generic-with-memoization)))
#+end_src

* Type ~list-of~
** Dependencies
#+begin_src elisp :results none
(require 'bytecomp)
#+end_src

** Prerequisites
*** proper-list-with-elements-of-type-p
**** Examples
***** Basic Examples
****** TEST-PASSED Check all list elements for given type
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-proper-list-with-elements-of-type-p 'symbol '(a b c))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

***** Scope
****** TEST-PASSED Check fails on improper lists
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-proper-list-with-elements-of-type-p 'symbol '(a . b))
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

***** Properties and Relations
****** TEST-PASSED All elements of empty list are of any type
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-proper-list-with-elements-of-type-p 'symbol nil)
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

**** Definition
#+begin_src elisp :results none
(defun akater-misc-proper-list-with-elements-of-type-p (type list)
  (declare (pure t) (side-effect-free t))
  (let ((result t))
    (while list
      (unless (and (consp list)
                   (cl-typep (pop list) type))
        ;; pnullf not defined yet, bad
        (cl-psetf list nil
                  result nil)))
    result))
#+end_src

**** Tests
***** TEST-PASSED Singleton list
#+begin_src elisp :tangle no :results code :wrap example elisp
(akater-misc-proper-list-with-elements-of-type-p 'symbol '(a))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

*** type-checkers/list-of
#+begin_src elisp :results none
(defvar akater-misc-essentials-type-checkers/list-of nil)
#+end_src

** Definition
#+begin_src elisp :results none
(cl-deftype list-of (type)
  `(and list
        (satisfies
         ,(with-memoization (alist-get type akater-misc-essentials-type-checkers/list-of
                                       nil nil #'equal)
            (let ((list (let ((gensym-counter 0)) (gensym "list-"))))
              (byte-compile
               `(lambda (,list)
                  ,(format "Check %s to be a (proper) list and check all its elements for type %s"
                           (upcase (symbol-name list)) type)
                  (akater-misc-proper-list-with-elements-of-type-p
                   ',type ,list))))))))
#+end_src

* make-keyword
** Notes
Used in both macs and plists-macs and the latter needs it at runtime.

** Definition
#+begin_src elisp :results none
(cl-eval-when (compile load)
  (defun make-keyword (symbol-or-string)
    "Convert SYMBOL-OR-STRING to keyword."
    (declare (pure t))
    (cl-etypecase symbol-or-string
      (keyword symbol-or-string)
      (symbol (make-keyword (symbol-name symbol-or-string)))
      (string (intern (concat ":" symbol-or-string))))))
#+end_src

* Type ~cl-cons~
This should go to some ~cl-macs-akater-extras~, I guess.

** Prerequisites
*** type-checkers/cl-cons
#+begin_src elisp :results none
(defvar akater-misc-essentials-type-checkers/cl-cons nil)
#+end_src

** Definition
#+begin_src elisp :results none
(cl-deftype cl-cons (x y)
  `(and cons
        (satisfies
         ,(with-memoization (alist-get x (alist-get y akater-misc-essentials-type-checkers/cl-cons
                                                    nil nil #'equal)
                                       nil nil #'equal)
            (let ((cons (let ((gensym-counter 0)) (gensym "cons-"))))
              (byte-compile
               `(lambda (,cons)
                  ,(format "Check if %s has car of type %s and cdr of type %s"
                           (upcase (symbol-name cons)) x y)
                  (and (cl-typep (car ,cons) ',x)
                       (cl-typep (cdr ,cons) ',y)))))))))
#+end_src
